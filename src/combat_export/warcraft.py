"""
We define our very own world of warcraft here
"""
from pprint import pprint

from prometheus_client import (
    Info,
    Enum,
    Counter,
)


def unit_type(flags):
    """Get unit type from flags"""
    if "TYPE_PLAYER" in flags:
        return "player"
    if "TYPE_PET" in flags:
        return "pet"
    if "TYPE_NPC" in flags:
        return "npc"

    return "unknown"


def strip_realm(name):
    """remove the realm from the name"""
    return name.split("-")[0]


class World:
    """Our world (of warcraft) holds players and mobs"""
    def __init__(self, registry):
        """Initialize our world"""
        self.registry = registry
        self.actions_counter = None
        self.damage_counter = None
        self.heal_counter = None
        self.reset()

    def reset(self):
        """Reset all metrics"""
        # We collect our stats here
        if self.actions_counter:
            self.registry.unregister(self.actions_counter)
        self.actions_counter = Counter(
            "wow_actions", "Total Actions",
            [
                "src_uid", "src_name", "src_type",
                "dst_uid", "dst_name", "dst_type",
                "result",
            ],
            registry=self.registry)

        if self.damage_counter:
            self.registry.unregister(self.damage_counter)
        self.damage_counter = Counter(
            "wow_damage", "Total Damage",
            [
                "src_uid", "src_name", "src_type",
                "dst_uid", "dst_name", "dst_type",
                "school",
            ],
            registry=self.registry)

        if self.heal_counter:
            self.registry.unregister(self.heal_counter)
        self.heal_counter = Counter(
            "wow_heal", "Total Heal",
            [
                "src_uid", "src_name", "src_type",
                "dst_uid", "dst_name", "dst_type",
            ],
            registry=self.registry)

        print("resetted combat log metrics")

    def update(self, event):
        """Update our world with an event"""
        if event["event"] == "COMBAT_LOG_START":
            return self.reset()
        if event["event"] == "SPELL_DAMAGE":  # priorities.
            return self._update_damage(event)
        if event["event"] == "SPELL_HEAL":
            return self._update_heal(event)
        if event["event"] == "SWING_DAMAGE":
            return self._update_damage(event)
        if event["event"] == "SPELL_MISSED":
            return self._update_miss(event)
        if event["event"] == "SWING_MISSED":
            return self._update_miss(event)

    def _update_damage(self, event):
        """Update the damage counters"""
        src_type = unit_type(event["sourceFlags"])
        src_uid = event["sourceGUID"]
        src_name = strip_realm(event["sourceName"])

        dst_type = unit_type(event["destFlags"])
        dst_uid = event["destGUID"]
        dst_name = strip_realm(event["destName"])

        school = event["school"][0]

        print("DAMAGE[{}] {}, {} -> {}".format(
            event["amount"],
            school,
            src_name,
            dst_name))

        # Increase counter
        self.damage_counter.labels(
            src_uid=src_uid,
            src_type=src_type,
            src_name=src_name,
            dst_uid=dst_uid,
            dst_type=dst_type,
            dst_name=dst_name,
            school=school).inc(event["amount"])

        self.actions_counter.labels(
            src_uid=src_uid,
            src_type=src_type,
            src_name=src_name,
            dst_uid=dst_uid,
            dst_type=dst_type,
            dst_name=dst_name,
            result="hit").inc()

    def _update_heal(self, event):
        """Update the damage counters"""
        src_type = unit_type(event["sourceFlags"])
        src_uid = event["sourceGUID"]
        src_name = strip_realm(event["sourceName"])

        dst_type = unit_type(event["destFlags"])
        dst_uid = event["destGUID"]
        dst_name = strip_realm(event["destName"])

        print("HEAL[{}] {} -> {}".format(
            event["amount"],
            src_name,
            dst_name))

        # Increase counter
        self.heal_counter.labels(
            src_uid=src_uid,
            src_type=src_type,
            src_name=src_name,
            dst_uid=dst_uid,
            dst_type=dst_type,
            dst_name=dst_name).inc(event["amount"])

        self.actions_counter.labels(
            src_uid=src_uid,
            src_type=src_type,
            src_name=src_name,
            dst_uid=dst_uid,
            dst_type=dst_type,
            dst_name=dst_name,
            result="hit").inc()

    def _update_miss(self, event):
        """A cast / swing / etc missed or failed"""
        src_type = unit_type(event["sourceFlags"])
        src_uid = event["sourceGUID"]
        src_name = strip_realm(event["sourceName"])

        dst_type = unit_type(event["destFlags"])
        dst_uid = event["destGUID"]
        dst_name = strip_realm(event["destName"])

        print("MISS {} -> {}".format(
            src_name,
            dst_name))

        self.actions_counter.labels(
            src_uid=src_uid,
            src_type=src_type,
            src_name=src_name,
            dst_uid=dst_uid,
            dst_type=dst_type,
            dst_name=dst_name,
            result="miss").inc()
