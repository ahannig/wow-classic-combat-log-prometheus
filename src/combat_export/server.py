"""
Prometheus export server
"""

import time
import os
from typing import Iterator
from datetime import datetime
from io import StringIO

from wowclp.wowclp import Parser
from prometheus_client import (
    CollectorRegistry,
    start_http_server,
)

from combat_export.warcraft import World


def follow_file(fhndl) -> Iterator[str]:
    """
    Yield each line from a file as they are written.
    Adopted from: https://stackoverflow.com/a/54263201
    """
    line = ""
    while True:
        tmp = fhndl.readline()
        if tmp is not None:
            line += tmp
        if line.endswith("\n"):
            yield line
            line = ""
        else:
            time.sleep(0.1)


def start(listen, logfilename):
    """Start the prometheus export server"""
    print(f"Starting http server @ {listen}")
    print(f"Reading logs from {logfilename}")

    # Start prometheus web server
    registry = CollectorRegistry(auto_describe=True)
    start_http_server(listen, registry=registry)

    # Watch logfile
    parser = Parser()
    world = World(registry=registry)

    with open(logfilename) as logfile:
        logfile.seek(0, os.SEEK_END)
        for line in follow_file(logfile):
            try:
                event = parser.parse_line(line)
            except Exception as err:
                print("error while parsing:")
                print(err)
                continue

            world.update(event)
