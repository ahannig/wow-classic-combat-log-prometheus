from utils.flags import get_spell_school


class SpellParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        spell = dict()
        spell['spellId'] = data[0]
        spell['spellName'] = data[1].replace("\"", "")
        spell['spellSchool'] = get_spell_school(data[2])

        return spell, data[3:]


class EnvironmentParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        env = dict()
        env['environmentalType'] = data[0]

        return env, data[1:]


class SwingParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        return {}, data


class EnchantmentParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        enchantment_data = dict()
        enchantment_data['spellName'] = data[0].replace("\"", "")
        enchantment_data['itemId'] = data[1]
        enchantment_data['itemName'] = data[2].replace("\"", "")

        return enchantment_data, data[3:]


class DummyPrefixParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        return {}, data
