from utils.flags import get_spell_school, get_power_type


class DamageParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        damage_data = dict()
        damage_data['amount'] = int(data[0])
        damage_data['overkill'] = int(data[1])
        damage_data['school'] = get_spell_school(data[2])
        damage_data['resisted'] = int(data[3])
        damage_data['blocked'] = int(data[4])
        damage_data['absorbed'] = int(data[5])
        damage_data['critical'] = (data[6] != 'nil')
        damage_data['glancing'] = (data[7] != 'nil')
        damage_data['crushing'] = (data[8] != 'nil')

        return damage_data


class MissParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        miss_data = dict()
        miss_data['missType'] = data[0]
        if len(data) > 1:
            miss_data['amountMissed'] = int(data[1])

        return miss_data


class HealParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        heal_data = dict()
        heal_data['amount'] = int(data[0])
        heal_data['overhealing'] = int(data[1])
        heal_data['absorbed'] = int(data[2])
        heal_data['critical'] = (data[3] != 'nil')

        return heal_data


class EnergizeParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        energize_data = dict()
        energize_data['amount'] = int(data[0])
        energize_data['powerType'] = get_power_type(int(data[1]))

        return energize_data


class DrainLeechParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        dl_data = dict()
        dl_data['amount'] = int(data[0])
        dl_data['powerType'] = get_power_type(int(data[1]))
        dl_data['extraAmount'] = int(data[2])

        return dl_data


class InterruptParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        interrupt_data = dict()
        interrupt_data['extraSpellId'] = data[0]
        interrupt_data['extraSpellName'] = data[1].replace("\"", "")
        interrupt_data['extraSchool'] = get_spell_school(data[2])

        return interrupt_data


class DispellParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        dispell_data = dict()
        dispell_data['extraSpellId'] = data[0]
        dispell_data['extraSpellName'] = data[1].replace("\"", "")
        dispell_data['extraSchool'] = get_spell_school(data[2])
        if len(data) > 3:
            dispell_data['auraType'] = data[3]

        return dispell_data


class StolenParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        stolen_data = dict()
        stolen_data['extraSpellId'] = data[0]
        stolen_data['extraSpellName'] = data[1].replace("\"", "")
        stolen_data['extraSchool'] = get_spell_school(data[2])
        stolen_data['auraType'] = data[3]

        return stolen_data


class ExtraAttacksParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        extra_attacks = dict()
        extra_attacks['amount'] = int(data[0])

        return extra_attacks


class AuraParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        aura_data = dict()
        aura_data['auraType'] = data[0]
        if len(data) > 1:
            aura_data['amount'] = int(data[1])

        return aura_data


class AuraBrokenParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        aura_data = dict()
        aura_data['extraSpellId'] = data[0]
        aura_data['extraSpellName'] = data[1].replace("\"", "")
        aura_data['extraSchool'] = get_spell_school(data[2])
        aura_data['auraType'] = data[3]

        return aura_data


class CastParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        cast_data = dict()
        if len(data) > 0:
            cast_data['failedType'] = data[0]

        return cast_data


class CreateSumResParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        return {}


class DummySuffixParser:
    def __init__(self, version="3.3.5"):
        self.version = version

    def parse(self, data):
        return {}
