#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime, time
from parsers.suffix_parsers import *
from parsers.prefix_parsers import *
from utils.event_handlers import EventHandlers
from utils.flags import get_unit_type


class CombatLogParser:
    def __init__(self, version="3.3.5"):
        self.version = version
        self.eh = EventHandlers(version)

    def parse_line(self, line, year=None):
        log_year = int(year) if year else datetime.datetime.today().year
        splitted_entry = line.decode('utf-8-sig').split(' ')

        if len(splitted_entry) < 4:
            raise Exception("Invalid Line : " + str(line))

        slug = '{2} {0[0]:02d}/{0[1]:02d} {1}'.format(list(map(int, splitted_entry[0].split('/'))),
                                                      splitted_entry[1][:-4],
                                                      log_year)
        date = datetime.datetime.strptime(slug, '%Y %m/%d %H:%M:%S')
        timestamp = time.mktime(date.timetuple()) + float(splitted_entry[1][-4:])

        data = " ".join(splitted_entry[3:]).strip().split(',')
        return self.parse_data(timestamp, data)

    def parse(self, path, limit=0, year=None):
        log_year = int(year) if year is not None else datetime.datetime.today().year

        with open(path, "rb") as f:
            if limit > 0:
                for entry in [next(f) for x in range(limit)]:
                    yield self.parse_line(entry, log_year)
            else:
                for entry in f:
                    yield self.parse_line(entry, log_year)

    def parse_data(self, timestamp, data):
        log = dict()
        event = data[0]
        if event == "COMBAT_LOG_VERSION":
            return {
                "event": "COMBAT_LOG_START",
                "timestamp": timestamp,
            }

        print(data)
        print(data[6])
        if len(data) < 7:
            raise Exception("Invalid Data : " + str(data))
        log['timestamp'] = timestamp
        log['event'] = event
        log['sourceGUID'] = data[1]
        log['sourceName'] = data[2].replace("\"", "")
        log['sourceFlags'] = get_unit_type(data[3])
        log['destGUID'] = data[4]
        log['destName'] = data[5].replace("\"", "")
        log['destFlags'] = get_unit_type(data[6])

        suffix = ""
        prefix = ""
        suffix_parser = None
        prefix_parser = None
        matches = list()

        for (event_prefix, parser) in self.eh.prefixes.items():
            if event.startswith(event_prefix):
                matches.append(event_prefix)

        if len(matches) > 0:
            prefix = max(matches, key=len)
            prefix_parser = self.eh.prefixes.get(str(prefix), DummyPrefixParser())
            suffix = event[len(prefix):]
            suffix_parser = self.eh.suffixes.get(suffix, DummySuffixParser())
        else:
            for (other_event, parser) in self.eh.other_events.items():
                if event == other_event:
                    prefix_parser, suffix_parser = self.eh.other_events.get(other_event,
                                                                            (DummyPrefixParser(), DummySuffixParser()))
                    break

        if not prefix_parser or not suffix_parser:
            print("Unknown event format : " + str(data))

        prefix_log, data = prefix_parser.parse(data[7:])
        log.update(prefix_log)
        log.update(suffix_parser.parse(data))

        return log


if __name__ == "__main__":
    import sys

    if len(sys.argv) == 1:
        raise ValueError("Missing filename.")

    p = CombatLogParser()
    logs = p.parse(sys.argv[1])

    for log in logs:
        print(log)
