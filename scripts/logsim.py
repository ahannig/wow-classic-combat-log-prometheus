"""Read line, write to output"""

import time

def _logsim(src, dst, factor):
    """Run the simulation"""
    t = 0
    while True:
        line = src.readline()
        if not line:
            return

        tokens = line.split(" ")
        if len(tokens) < 3:
            continue

        # decode timestamp
        tm = tokens[1].split(":")
        sec = float(tm[2])

        print(line)
        dst.write(line)
        dst.flush()
        wait = abs(t-sec)
        t = sec
        time.sleep(wait * (1.0 / factor))


def logsim(srcf, dstf, factor=1.0):
    """Simulate the logging"""
    with open(srcf) as src:
        with open(dstf, "a") as dst:
            _logsim(src, dst, factor)


if __name__ == "__main__":
    logsim("WoWCombatLog.txt", "livelog.txt", 20)
