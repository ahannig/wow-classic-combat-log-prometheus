#!/usr/bin/env python3

"""
WoW combat log prometheus exporter

This tool watches the combat log and aggregates events for
prometheus
"""

import sys
import os

# Prepare path and start export application
app_path = os.path.abspath(
    os.path.dirname(__file__))
sys.path.insert(
    0, os.path.join(app_path, "vendor"))
sys.path.insert(
    0, os.path.join(app_path, "src"))


from combat_export import server


def main(logfilename):
    """Combat Log Prometheus Exporter"""
    if not logfilename:
        print("usage: ./export.py <path to WoWCombatlog.txt>")
        return

    server.start(9100, logfilename)


if __name__ == "__main__":
    main(*sys.argv[1:])
